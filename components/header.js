import Link from "next/link";
import makeid from "../utils/random";

const Header = () => {
  return (
    <header>
      <ul>
        <li>
          <Link href="/">
            <a>Home</a>
          </Link>
        </li>
        <li>
          <Link href="/about">
            <a>About</a>
          </Link>
        </li>
        <li>
          <Link href={`/post/1${makeid(10)}`}>
            <a>First CODE FATHER</a>
          </Link>
        </li>
        <li>
          <Link href={`/post/2${makeid(10)}`}>
            <a>Second CODE FATHER</a>
          </Link>
        </li>
      </ul>
    </header>
  );
};

export default Header;
