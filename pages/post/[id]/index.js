import { useRouter } from "next/router";
import Link from "next/link";
import Header from "../../../components/header";
import makeid from "../../../utils/random";

const Post = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <Header />
      <h1>Post: {id}</h1>
      <ul>
        <li>
          <Link href="/post/[id]/[code]" as={`/post/${id}/${makeid(30)}`}>
            <a>First CODE</a>
          </Link>
        </li>
        <li>
          <Link href="/post/[id]/[code]" as={`/post/${id}/${makeid(30)}`}>
            <a>Second CODE</a>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default Post;
