import { useRouter } from "next/router";
import Header from "../../../components/header";

const Comment = () => {
  const router = useRouter();
  const { id, code } = router.query;

  return (
    <>
      <Header />
      <h1>Post: {id}</h1>
      <h1>CODE: {code}</h1>
    </>
  );
};

export default Comment;
